import 'babel-polyfill';
import Vue from 'vue'
window._ = require('lodash');
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import store from './store'
import VueRouter from 'vue-router'
import ls from './helpers/localStorage'

import ProductPage from './pages/ProductPage'
import Catalog from './pages/Catalog'
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Catalog },
    { path: '/product/:id', component: ProductPage }
  ]
})

Vue.filter('currency', function (val) {
  return `$${val}`;
});

const app = new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})

app.$store.watch(
  (state) => state.products.order,
  (val) => ls.set('ORDER', val),
  { deep: true }
);



