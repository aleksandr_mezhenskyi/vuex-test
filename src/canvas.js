const InputController = function (target) {
    const DIRECTIONS = {
        UP: 38,
        DOWN: 40,
        LEFT: 37,
        RIGHT: 39
    }
    InputController.prototype.onKeyup = function (dir, callback) {
        if (DIRECTIONS.hasOwnProperty(dir)) {
            document.addEventListener('keyup', (e) => {
                if (e.keyCode === DIRECTIONS[dir]) callback();
            })
        } else {
            throw new Error('No direction type found!')
        }
    }
};



export const Snake = (function (canvasId) {
    let canvas,
        cxt,
        shakeParts = 3,
        shakePartWidth = 9,
        shakePartHeight = 9;

    let state = {
        currentX: 0,
        currentY: 0,
        direction: 'RIGHT',
        get isGameOn() {
            return this.currentX < canvas.width && this.currentY < canvas.height;
        }
    }
    const inputController = new InputController(canvas);
    inputController.onKeyup('UP', () => state.direction = 'UP');
    inputController.onKeyup('DOWN', () => state.direction = 'DOWN');
    inputController.onKeyup('LEFT', () => state.direction = 'LEFT');
    inputController.onKeyup('RIGHT', () => state.direction = 'RIGHT');

    let i = 0;
    function render(time) {
        i++;
        cxt.clearRect(0, 0, canvas.width, canvas.height);
        drawParts(state.direction, i / 100);
        if (state.isGameOn) {
            window.requestAnimationFrame(render);
        } else {
            alert('Gave over');
            state.currentX = 0;
            state.currentY = 0;
            // i = 0;
            render();
        }
    }

    const drawParts = (dir, step) => {
        let i = shakeParts;
        switch (dir) {
            case ('DOWN'):
                state.currentY += step;
                while (i > 0) {
                    cxt.fillRect(state.currentX, state.currentY + i + i * shakePartHeight, shakePartWidth, shakePartHeight);
                    i--;
                }
                break;
            case ('LEFT'):
                state.currentX -= step;
                while (i > 0) {
                    cxt.fillRect(state.currentX + i + i * shakePartWidth, state.currentY, shakePartWidth, shakePartHeight);
                    i--;
                }
                break;
            case ('UP'):
                state.currentY -= step;
                while (i > 0) {
                    cxt.fillRect(state.currentX, state.currentY + i + i * shakePartHeight, shakePartWidth, shakePartHeight);
                    i--;
                }
                break;
            case ('RIGHT'):
                state.currentX += step;
                while (i > 0) {
                    cxt.fillRect(state.currentX + i + i * shakePartWidth, state.currentY, shakePartWidth, shakePartHeight);
                    i--;
                }
                break;
            default:
                break;
        }


    }

    function moveDown(currentX, currentY) {
        drawParts('DOWN', currentX, currenY);
    }

    function init() {
        canvas = document.getElementById(canvasId || 'canvas');
        if (!canvas) throw new Error('No canvas found')
        canvas.width = window.outerWidth / 2;
        canvas.height = window.innerHeight - 100;
        canvas.style.border = '3px solid green';
        document.body.style.overflow = 'hidden';
        cxt = canvas.getContext('2d');
        render();
    }
    return {
        init
    }
})();