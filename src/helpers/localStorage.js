
export function set(key, value) {
    if (key === undefined || value === undefined) {
        console.warn('Key and value can\'t be undefined')
        return;
    }

    value = JSON.stringify(value);
    localStorage.setItem(key, value);
}

export function get(key) {
    if (!key) return;

    let value = localStorage.getItem(key);
    try {
        return JSON.parse(value)
    } catch (e) {
        return value;
    }
}

export function remove(key) {
    if (!key) return;
    localStorage.removeItem(key);
}

export default (function () {
    return Object.freeze({
        get,
        remove,
        set
    })
}())