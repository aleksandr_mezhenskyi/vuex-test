import API from '../../api'
import Vue from 'vue'
import _ from 'lodash'
import ls from '../../helpers/localStorage'

const state = {
    buffer: {},

    order: {},

    resentViewed: []
}

const PRODUCTS_VIEWS = {
    OFTEN: 'often',
    CATALOG: 'catalog',
    ADDITIONAL: 'additional'
};

Object.defineProperty(PRODUCTS_VIEWS, 'RESENT', {
  value: 'resent',
  enumerable: false
});

const getters = {
    ...createViewsGetters(),

    getProduct: (state) => (id) => state.buffer[id] || {},

    getResent: (state) => (id) => _.filter(state.buffer, (p, key) => p.views.includes('resent') && key !== id),

    getAdditional: (state) => _.filter(state.buffer, p => p.views.includes(PRODUCTS_VIEWS.ADDITIONAL)),
    // getMainOrder: (state) => _.filter(state.buffer, p => p.inOrder && !p.views.includes(PRODUCTS_VIEWS.ADDITIONAL)),
    getMainOrder: (state) => _.filter(state.order, p => !p.views.includes(PRODUCTS_VIEWS.ADDITIONAL)),

    getAdditionalOrder: (state) => _.filter(state.order, p => p.views.includes(PRODUCTS_VIEWS.ADDITIONAL)),

    getTottal: (state, getters) => {
        if (!getters.getMainOrder.length) return 0;

        return _.reduce(state.order, (sum, p) => {
            return sum + p.totalPrice;
        }, 0);
    }
}

/**
 * Mutations
 */
const mutations = {
    ADD_PODUCTS(state, { data: products, view = PRODUCTS_VIEWS.CATALOG }) {

        _.forEach(products, (p, id) => {
            const product = state.buffer[id];

            if (!product) {
                p = addReactivityRules(p, view);
                Vue.set(state.buffer, id, p);
            } else {
                product.views.push(view);
                if (p.increment) product.increment = p.increment;
            }
        });
    },

    INCREMENT(state, id) {
        const product = state.buffer[id];
        if (product && product.increment < 10) product.increment++;
    },

    DECREMENT(state, id) {
        const product = state.buffer[id];
        if (product && product.increment > 1) product.increment--;
    },

    BUY_PRODUCT(state, id) {
        const product = state.buffer[id];
        if (product) {
            Vue.set(state.order, id, product);
        }
    },

    DELETE_PRODUCT(state, id) {
        const product = state.buffer[id];

        if (product) {
            product.increment = 1;
            Vue.delete(state.order, id);
        }
    },

    ADD_RESENT(state, id) {
        const product = state.buffer[id];
        if (product && !product.views.includes('resent')) {
            product.views.push('resent');

            state.resentViewed.push(id);
            ls.set('RESENT', state.resentViewed);
        }
    },

    SET_RESENT(state, ids) {
        state.resentViewed = ids;
    }
}

/**
 * actions
 */
const actions = {
    async getCatalog({ commit }) {
        const { data } = await API.get('/catalog');
        commit('ADD_PODUCTS', { data, view: PRODUCTS_VIEWS.CATALOG });
    },

    async getOften({ commit }) {
        const { data } = await API.get('/often');
        commit('ADD_PODUCTS', { data, view: PRODUCTS_VIEWS.OFTEN });
    },

    async getAdditional({ commit }) {
        const { data } = await API.get('/additional');
        commit('ADD_PODUCTS', { data, view: PRODUCTS_VIEWS.ADDITIONAL });
    },

    async getResent({ commit }) {
        let ids = ls.get('RESENT');

        if (ids && ids.length) {
            commit('SET_RESENT', ids);

            ids = ids.join(',');
            const { data } = await API.get('/products/' + ids);
            commit('ADD_PODUCTS', { data, view: 'resent' });
        }
    },

  async getOrder({ commit }) {
    let order = ls.get('ORDER');

    if (order) {
      let ids = Object.keys(order).join(',');
      const { data } = await API.get('/products/' + ids);

      commit('ADD_PODUCTS', { data });

      _.forIn(data, (p, id) => {
        p.increment = order[id].increment;
        commit('BUY_PRODUCT', id);
      });
    }
  },

    async getProduct({ commit }, id) {
        const { data } = await API.get('/product/' + id);
        commit('ADD_PODUCTS', { data });
    },
}

/**
 * 
 * Helpers
 * 
 */
function addReactivityRules(product, view) {
    Vue.set(product, 'increment', product.increment || 1);

    product.totalWeight = product.weight;
    product.totalPrice = product.price;

    Object.defineProperty(product, 'totalWeight', {
        get: function () {
            return this.increment * this.weight;
        }
    });

    Object.defineProperty(product, 'totalPrice', {
        get: function () {
            return this.increment * this.price;
        }
    });

    Vue.set(product, 'views', [view]);
    Vue.set(product, 'inOrder', false);
    return product;
}

function createViewsGetters() {
    const getters = {};

    _.forEach(PRODUCTS_VIEWS, (view) => {
        getters[`get${_.upperFirst(view)}`] = (state) => _.filter(state.buffer, p => p.views.includes(view));
    })

    return getters;
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
