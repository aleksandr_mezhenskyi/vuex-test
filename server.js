var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cors = require('cors');

var express = require('express');
var app = express();
app.use(cors());

const routes = ['catalog', 'often', 'secondary', 'additional'];

routes.forEach(route => {
    app.get(`/${route}`, function (req, res) {
        res.send(require('./data/' + route + '.json'));
    });
});

app.get(`/product/:id`, function (req, res) {
    const data = require('./data/catalog.json');
    const { id } = req.params;
    const product = {};
    product[id] = data[id]
    res.send(product);
});

app.get(`/products/:ids`, function (req, res) {
    const data = require('./data/catalog.json');
    let { ids } = req.params;
    ids = ids.split(',');

    const products = {};
    ids.forEach(id => products[id] = data[id]);
    res.send(products);
});

var port = process.env.PORT || 4000;

app.listen(port, () => {
    console.log('Listening on port ' + port);
});


